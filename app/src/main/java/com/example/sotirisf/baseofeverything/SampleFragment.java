package com.example.sotirisf.baseofeverything;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;

/**
 * Created by Suleiman on 28/02/17.
 */

public class SampleFragment extends BaseFragment {

    @BindView(R.id.txt) TextView mTextView;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTextView.setText("Make sure you interact with UI widgets in this method!");

    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_layout;
    }
}
