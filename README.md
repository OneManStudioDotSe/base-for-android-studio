# base-for-android-studio


## Already integrated

- https://android.jlelse.eu/how-i-reduced-my-android-build-times-by-89-4242e51ce946?gi=9efd33598f7
- https://www.reddit.com/r/androiddev/comments/7sxhig/android_studio_slower_when_using_kotlin/dt88pgn/
- https://developer.android.com/topic/libraries/support-library/packages#v7-appcompat
- https://developer.android.com/studio/build/multidex


## To be integrated

- https://developer.android.com/topic/libraries/architecture/navigation/
- https://developer.android.com/training/sharing/shareaction
- https://developer.android.com/guide/topics/permissions/overview
- https://developer.android.com/guide/topics/ui/notifiers/notifications